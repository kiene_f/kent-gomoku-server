// import
var Utils = require('./utils.js');
var Game = require('./models/Game.js');

var cluster = require( "cluster" );   //Require the cluster module
var http  = require( "http" );    //Require the http module
var ws    = require( "ws" ).Server; //Require the WebSocket server module
// list of all the games
var games = [];

var workers = new Array();      //Array of worker threads

if( cluster.isMaster ){
  var cpus  = 20;  //Number of cores on this host
  for( var i = 0; i < cpus; i ++){
    var thread  = cluster.fork();   //Fork

    workers.push( thread );   //Add the thread to the array of workers
  }
  return;   //Prevent the master thread from executing past this point
}

var server  = http.createServer().listen( 1337 ); //Start the server on TCP 8080
var wss   = new ws( {server: server} );   //Create the WebSocket server and bind it to the http server

var connections = new Array();    //Array of active connections

process.on( "message", function( message ){ //On an IPC message
  connections.forEach( function( ws ){  //Loop through the active connections
    ws.send( message );   //Send the message
  });
});

wss.on( "connection", function( ws ){ //On a new WebSocket connection
  ws.send( "Connected to #"+ process.pid ); //Send the current pid to the client
  ws.on( "message", function( message ){    //On a message from the client
    var receive = JSON.parse(message);
    console.log(receive);

    // First connection
    if (receive.hasOwnProperty('connection'))
    {
      receive = receive.connection;
      // Create
      if (receive.Game.join == false)
      {
        var game = new Game(receive.Game.id, receive.Game.type);
        game.createBoard(receive.Board.size);
        var player = game.addPlayer(receive.Player.id, receive.Player.name, receive.Player.type, ws);
        games.push(game);
        delete(game);
        delete(player);
      }
      // Join
      else
      {
        var game = games.find(o => o.id == receive.Game.id);
        if (typeof game !== "undefined")
        {
          var player = game.addPlayer(receive.Player.id, receive.Player.name, receive.Player.type, ws);
          delete(player);
        }
        else
          Utils.sendError(connection, "connection", "Connection: Game ID doesn't exist.");
        delete(game);
      }
    }
    // Each move
    else if (receive.hasOwnProperty('move'))
      {
        receive = receive.move;
        var game = games.find(o => o.id == receive.Game.id);
        if (typeof game !== "undefined")
          game.play(receive.Player.id, receive.Coord);
        else
          Utils.sendError(connection, "move", "Move: Game ID doesn't exist.");
        delete(game);
      }
    // Reload game
    else if (receive.hasOwnProperty('reload'))
    {
      receive = receive.reload;
      var game = games.find(o => o.id == receive.Game.id);
      if (receive.Player)
        game.tryToReload(receive.Player.id);
      else if (typeof game !== "undefined")
        game.reload();
      else
        Utils.sendError(connection, "reload", "Reload: Game ID doesn't exist.");
      delete(game);
    }
    process.send( {pid: process.pid, msg: message} ); //Send the message to the master process
  });
  ws.on( "close", function(){ //On a closed connection
    var index = connections.indexOf( ws );  //Get the index of the connection in the array of active connections
    connections.splice( index, 1 );   //Remove the index from the array
  });
  connections.push( ws );   //Add the connection to the array of active connections
});