module.exports = {
	/**
	 * Helper function for escaping input strings
	 */
	htmlEntities: function (str) {
		return String(str)
			.replace(/&/g, '&amp;').replace(/</g, '&lt;')
			.replace(/>/g, '&gt;').replace(/"/g, '&quot;');
	},

	sendError: function(connection, type, message)
	{
		var send = {};
		send[type] = {
				Result: {
					success: false,
					message: message
					}
				};
		connection.sendUTF(JSON.stringify(send));
	},

	clone: function(obj)
	{
		return JSON.parse(JSON.stringify(obj));
	},

	getReverseColor: function(color) {
		if (color == "white")
			return "black";
		else
			return "white";
	},

	coord(x, y) {
		var coord = {x:x, y:y};

		return coord;
	}
};
