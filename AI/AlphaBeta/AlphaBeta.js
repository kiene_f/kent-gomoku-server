Player = require('../../models/Player.js');
Board = require('../../models/Board.js');
Ruler = require('../../models/Ruler.js');
Utils = require('../../utils.js');

function AlphaBeta() {
	this.ruler = new Ruler();
    this.heuristic = new Heuristic();
    this.depth = 1;
}

AlphaBeta.prototype = {
	play: function(boardA, AIcolor) {
		var boardB = boardA.clone();
		this.heuristic.get(boardA, AIcolor);
		this.heuristic.get(boardB, Utils.getReverseColor(AIcolor)); // Remplir les 2 board
		var boxListA = this.getBoxListWithCost(boardA);
		var boxListB = this.getBoxListWithCost(boardB); // Faire les 2 listes de move
		var boxList = boxListA.concat(boxListB);
		var finalBox = new Box();
		var tmpcost = 0;
		var bestCost = -1;
		var coord = {x:-1,y:-1};

		if ((win = this.checkWinningMove(boxList))[0] == true) {
			return win[1];
		}

		for (var i = 0; i < boxList.length; i++) {
			var nextBoard = boardA.clone();
			coord = Utils.coord(boxList[i].x, boxList[i].y);

			nextBoard.addBox(coord, AIcolor);
			tmpCost = this.alphaBeta(nextBoard, Utils.getReverseColor(AIcolor), this.depth, true, boxList[i].cost, bestCost);
			if (tmpCost > bestCost) {
				bestCost = tmpCost;
				finalBox.x = boxList[i].x;
				finalBox.y = boxList[i].y;
			}
		}
		if (finalBox.x == -1 || finalBox.y == -1)
			finalBox = this.getFirstEmptyBox(boardA);
		return finalBox;
	},

	alphaBeta: function(boardA, color, depth, min, nodeCost, topValue) {
		var boxListA = [];
		var boxListB = [];
		var boxList = [];
		var coord = {x:-1,y:-1};
		var bestCost = 0;
		var tmpCost;

		var boardB = boardA.clone();
		this.heuristic.get(boardA, color);
		this.heuristic.get(boardB, Utils.getReverseColor(color));

		if (depth <= 0 || nodeCost >= 39) {
			return nodeCost;
		} else {
			boxListA = this.getBoxListWithCost(boardA);
			boxListB = this.getBoxListWithCost(boardB);
			boxList = boxListA.concat(boxListB);

			for (var i = 0; i < boxList.length; i++) {
				var nextBoard = boardA.clone();
				coord = Utils.coord(boxList[i].x, boxList[i].y);
				nextBoard.addBox(coord, Utils.getReverseColor(color));
				if (min) {
					// alphaBeta
					if (bestCost != 0 && topValue != -1 && topValue >= bestCost)
						return bestCost;
					tmpCost = this.alphaBeta(nextBoard, Utils.getReverseColor(color), depth - 1, !min, boxList[i].cost, bestCost);
					if (tmpCost > 0 && (tmpCost < bestCost || bestCost <= 0))
						bestCost = tmpCost;
				} else {
					// alphaBeta
					if (bestCost != 0 && topValue != -1 && topValue <= bestCost)
						return bestCost;
					tmpCost = this.alphaBeta(nextBoard, Utils.getReverseColor(color), depth - 1, !min, boxList[i].cost, bestCost);
					if (tmpCost > 0 && tmpCost > bestCost)
						bestCost = tmpCost;
				}
			}
			return bestCost;
		}
	},

    checkWinningMove: function(boxList) {
		for (var i = 0; i < boxList.length; i++) { // First check to see winning move (If move makes a 5 in a row)
			if (boxList[i].cost >= 40) {
				return [true, boxList[i]];
			}
		}

		for (var i = 0; i < boxList.length; i++) { // 2nd check to see unbeaten move (If move makes a 4 in a row with 2 locations)
			if (boxList[i].cost == 39) {
				return [true, boxList[i]];
			}
		}
		return [false, new Box()];
	},

	getFirstEmptyBox: function(board) {
        var box = new Box();

		for (var y = 0; y < board.size; y++) {
			for (var x = 0; x < board.size; x++) {
				if (board.map[y][x].value == 0) {
					box.x = x;
					box.y = y;
					return box;
				}
			}
		}
		return box;
	},

	getBoxListWithCost: function(board) {
        var boxList = [];

        for (var y = 0; y < board.size; y++) {
            for (var x = 0; x < board.size; x++) {
                if (board.map[y][x].cost > 0) {
                    var box = new Box();
                    box.x = x;
                    box.y = y;
                    box.cost = Utils.clone(board.map[y][x].cost);
                    boxList.push(box);
                }
            }
        }
        return boxList;
    }
}

module.exports = AlphaBeta;
