// import
var Utils = require('./utils.js');
var Game = require('./models/Game.js');

"use strict";
process.title = 'node-chat';

var webSocketsServerPort = 1337;
var webSocketServer = require('websocket').server;
var http = require('http');

// latest 100 messages
var history = [];
// list of currently connected clients (users)
var clients = [];
// list of all the games
var games = [];


/**
 * HTTP server
 */
var server = http.createServer(function(request, response) {
});

server.listen(webSocketsServerPort, function() {
  console.log((new Date()) + " Server is listening on port "
      + webSocketsServerPort);
});


/**
 * WebSocket server
 */
var wsServer = new webSocketServer({
  httpServer: server
});

// This callback function is called every time someone tries to connect to the WebSocket server
wsServer.on('request', function(request) {
  console.log((new Date()) + ' Connection from origin '
      + request.origin + '.');
  // accept connection - you should check 'request.origin' to
  // make sure that client is connecting from your website
  // (http://en.wikipedia.org/wiki/Same_origin_policy)
  var connection = request.accept(null, request.origin);
  // we need to know client index to remove them on 'close' event
  var index = clients.push(connection) - 1;
  console.log((new Date()) + ' Connection accepted.');
  // send back chat history
  /*if (history.length > 0) {
    connection.sendUTF(
        JSON.stringify({ type: 'history', data: history} ));
  }*/

  // user sent some message
  connection.on('message', function(message) {
    if (message.type === 'utf8') {
        console.log('receive =' + message.utf8Data);
        var receive = JSON.parse(message.utf8Data);

        // First connection
        if (receive.hasOwnProperty('connection'))
        {
          receive = receive.connection;
          // Create
          if (receive.Game.join == false)
          {
            var game = new Game(receive.Game.id, receive.Game.type);
            game.createBoard(receive.Board.size);
            var player = game.addPlayer(receive.Player.id, receive.Player.name, receive.Player.type, connection);
            games.push(game);
            delete(game);
            delete(player);
          }
          // Join
          else
          {
            var game = games.find(o => o.id == receive.Game.id);
            if (typeof game !== "undefined")
            {
              var player = game.addPlayer(receive.Player.id, receive.Player.name, receive.Player.type, connection);
              delete(player);
            }
            else
              Utils.sendError(connection, "connection", "Connection: Game ID doesn't exist.");
            delete(game);
          }
        }
        // Each move
        else if (receive.hasOwnProperty('move'))
          {
            receive = receive.move;
            var game = games.find(o => o.id == receive.Game.id);
            if (typeof game !== "undefined")
              game.play(receive.Player.id, receive.Coord);
            else
              Utils.sendError(connection, "move", "Move: Game ID doesn't exist.");
            delete(game);
          }
        // Reload game
        else if (receive.hasOwnProperty('reload'))
        {
          receive = receive.reload;
          var game = games.find(o => o.id == receive.Game.id);
          if (receive.Player)
            game.tryToReload(receive.Player.id);
          else if (typeof game !== "undefined")
            game.reload();
          else
            Utils.sendError(connection, "reload", "Reload: Game ID doesn't exist.");
          delete(game);
        }
        // we want to keep history of all sent messages
        var obj = {
          time: (new Date()).getTime(),
          text: Utils.htmlEntities(message.utf8Data),
        };


        history.push(obj);
      }
    });


  // user disconnected
  connection.on('close', function(connection) {
      console.log((new Date()) + " Peer "
          + connection.remoteAddress + " disconnected.");
      // remove user from the list of connected clients
      clients.splice(index, 1);
  });
});
