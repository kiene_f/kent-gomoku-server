function Box() {
	this.value = 0; // 0 = None, 1 = White, 2 = Black
	this.cost = 0;
	this.x = -1;
	this.y = -1;
	this.type = 0;
	this.pair = 0;
	this.location = 1;
}


Box.prototype = {
	display: function() {
		console.log("Box: x:" + this.x + " | y:" + this.y + " | cost:" + this.cost + " | value:" + this.value)
	}
};

module.exports = Box;
