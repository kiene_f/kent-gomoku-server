function Player(id, name) {
  this.id = id;
  this.name = name;
  this.color;
  this.win = "none";
  this.nbrWin = 0;
  this.connection;
  this.type;
}

module.exports = Player;
