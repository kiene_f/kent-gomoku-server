Box = require('./Box.js');
Utils = require('../utils.js');

function Board(size) {
	this.size = size;
	this.map = [];
	this.createBoxes(size);
};

Board.prototype = {
	clone: function() {
		var clone = new Board(this.size);
		clone.map = JSON.parse(JSON.stringify(this.map));
		return clone;
	},

	resetMap: function() {
		for(var y = 0; y < this.size; y++) {
			for(var x = 0; x < this.size; x++) {
				this.map[y][x].value = 0;
				this.map[y][x].cost = 0;
			}
		}
	},

	resetMapCost: function() {
		for(var y = 0; y < this.size; y++) {
			for(var x = 0; x < this.size; x++) {
				this.map[y][x].cost = 0;
			}
		}
	},


	createBoxes: function(size) {
		 for(var y = 0; y < size; y++) {
			 this.map.push([]);
			 for(var x = 0; x < size; x++) {
				 this.map[y].push(new Box());
			}
  		}
	},

	addBox: function(coord, color) {
		if (color == "white") {
			this.map[coord.y][coord.x].value = 1;
		} else if (color == "black") {
			this.map[coord.y][coord.x].value = 2;
		}
	},

	displayBoard: function() {
		var str = "";

		for(var y = 0; y < this.size; y++) {
			for(var x = 0; x < this.size; x++) {
				str += "[" + this.map[y][x].value + "]";
		   }
		   str += "\n";
	   }
	   console.log(str);
	},

	displayBoardCost: function() {
		var str = "";

		for(var y = 0; y < this.size; y++) {
			for(var x = 0; x < this.size; x++) {
				if (this.map[y][x].cost > 0)
					str += "[" + this.map[y][x].cost + "]";
				else
					str += "[  ]";
		   }
		   str += "\n";
	   }
	   console.log(str);
	},

	hasMax: function() {
		for(var y = 0; y < this.size; y++) {
			for(var x = 0; x < this.size; x++) {
				if (this.map[y][x].cost >= 40)
					return (true);
			}
		}
		return (false);
	}
};

module.exports = Board;
