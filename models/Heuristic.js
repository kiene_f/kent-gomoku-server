Box = require('./Box.js');
Board = require('./Board.js');

function Heuristic() {
	this.board;
	this.color;
	this.value;
	this.opponent;
	this.boxList;
}

Heuristic.prototype = {
	get: function(board, color) {
		this.board = board;
		this.board.resetMapCost();
		this.color = color;
		this.boxList = [];
		if (color == "white")
		{
			this.value = 1;
			this.opponent = 2;
		}
		else if (color == "black")
		{
			this.opponent = 1;
			this.value = 2;
		}
		//console.log("check4axis");
		this.check4axis();
		//console.log("checkPair");
		this.checkPair();
		//console.log("additionOfCost");
		this.additionOfCost();
		//console.log("setBoardCost");
		this.setBoardCost();
		//console.log("Fin");
		board.displayBoardCost();
	},

	getByCoord: function(x, y, board, color) {
		var box = board.map[y][x];
		this.board = board;
		this.color = color;
		this.boxList = [];
		if (color == "white")
		{
			this.value = 1;
			this.opponent = 2;
		}
		else if (color == "black")
		{
			this.opponent = 1;
			this.value = 2;
		}
		this.check4axisByCoord(x, y);

		this.checkPair();

		this.additionOfCost();

		for (var i = 0, len = this.boxList.length; i < len; i++)
			if (this.boxList[i])
				box.cost = this.boxList[i].cost;

		return (box);
	},

	check4axisByCoord: function(x, y) {
		if (this.board.map[y][x].value == 0)
		{
			this.checkXaxis(x, y);
			this.checkYaxis(x, y);
			this.checkDiagLeftaxis(x, y);
			this.checkDiagRightaxis(x, y);
		}
	},

	check4axis: function() {
		for (var y = 0; y < this.board.size; y++)
		{
			for (var x = 0; x < this.board.size; x++)
			{
				if (this.board.map[y][x].value == 0)
				{
					this.checkXaxis(x, y);
					this.checkYaxis(x, y);
					this.checkDiagLeftaxis(x, y);
					this.checkDiagRightaxis(x, y);
				}
			}
		}
	},

	setBoardCost: function() {
		for (var i = 0, len = this.boxList.length; i < len; i++) {
			if (this.boxList[i])
			{
				this.board.map[this.boxList[i].y][this.boxList[i].x].cost = this.boxList[i].cost;
			}
		}
	},

	additionOfCost: function() {
		var duplicates = 1;
		while (duplicates == 1)
		{
			duplicates = 0;
			for (var i = 0, len = this.boxList.length; i < len; i++) {
				for (var j = 0; j < len; j++) {
					if (this.boxList[i] && this.boxList[j]
						&& this.boxList[i].x == this.boxList[j].x
						&& this.boxList[i].y == this.boxList[j].y
						&& i != j)
					{
						duplicates = 1;
						if (this.boxList[i].cost > this.boxList[j].cost)
						{
							if (this.boxList[i].cost < 40 && this.boxList[i].cost + (this.boxList[j].cost / 10) <= 39 && Math.floor((this.boxList[i].cost + (this.boxList[j].cost / 10)) / 10) < Math.floor(this.boxList[i].cost / 10) + 1)
							{
								this.boxList[i].cost = this.boxList[i].cost + (this.boxList[j].cost / 10);
								delete this.boxList[j];
							}
							else
								delete this.boxList[j];
						}
						else
						{
							if (this.boxList[j].cost < 40 && this.boxList[j].cost + (this.boxList[i].cost / 10) <= 39 && Math.floor((this.boxList[j].cost + (this.boxList[i].cost / 10)) / 10) < Math.floor(this.boxList[j].cost / 10) + 1)
							{
								this.boxList[j].cost = this.boxList[j].cost + (this.boxList[i].cost / 10);
								delete this.boxList[i];
							}
							else
								delete this.boxList[i];
						}
					}
				}
			}
		}
	},

	checkPair: function() {
		var sorted_boxList = this.boxList;

		for (var i = 0, len = sorted_boxList.length; i < len; i++) {
			if (sorted_boxList[i] && sorted_boxList[i].pair == 0)
			{
				for (var j = 0; j < len; j++) {
					if (sorted_boxList[j]
						&& sorted_boxList[i].cost == sorted_boxList[j].cost
						&& sorted_boxList[i].x == sorted_boxList[j].x
						&& sorted_boxList[i].y == sorted_boxList[j].y
						&& sorted_boxList[i].location == sorted_boxList[j].location
						&& i != j)
					{
						sorted_boxList[i].pair += 1;
						delete sorted_boxList[j];
					}
				}
			}
		}
		for (var i = 0, len = sorted_boxList.length; i < len; i++) {
			if (sorted_boxList[i] && sorted_boxList[i].pair > 0 && sorted_boxList[i].type > 0)
				this.setPairCost(sorted_boxList[i]);
		}
	},

	setPairCost: function(box)
	{
		var oneLocationCost = [
								[19, 24, 29],
								[29, 35, 39],
								[39, 39, 39],
								[40, 40, 40]
							  ];
		var twoLocationCost = [
								[29, 35, 39],
								[39, 39, 39],
								[39, 39, 39],
								[40, 40, 40]
							  ];
		if (box.location == 1 && box.type - 1 < oneLocationCost.length && box.pair - 1 < oneLocationCost[box.type - 1].length)
			box.cost = oneLocationCost[box.type - 1][box.pair - 1];
		else if (box.location == 2 && box.type - 1 < twoLocationCost.length && box.pair - 1 < twoLocationCost[box.type - 1].length)
			box.cost = twoLocationCost[box.type - 1][box.pair - 1];
	},

	displayBoxList: function(boxList)
	{
		for (var i = 0, len = boxList.length; i < len; i++) {
			if (boxList[i] && boxList[i].cost > 0)
				console.log("x="+boxList[i].x+" y="+boxList[i].y+" cost="+boxList[i].cost+ " type="+boxList[i].type+ " pair=" + boxList[i].pair+ " location=" + boxList[i].location);
		}
	},

	checkXaxis: function(x, y) {
		// before
		var b;
		// after
		var a;
		var location = new Object();
		var type = new Object();
		var tmpX;

		// Number of location
		//Before
		tmpX = x - 1;
		b = 0;
		while (tmpX >= 0 && this.board.map[y][tmpX].value != this.opponent)
		{
			b++;
			tmpX--;
		}
		//After
		tmpX = x + 1;
		a = 0;
		while (tmpX < this.board.size && this.board.map[y][tmpX].value != this.opponent)
		{
			a++;
			tmpX++;
		}
		location.b = b;
		location.a = a;
		location.total = a + b + 1;

		// Type
		// Before
		tmpX = x - 1;
		b = 0;
		while (tmpX >= 0 && this.board.map[y][tmpX].value == this.value)
		{
			b++;
			tmpX--;
		}
		// After
		tmpX = x + 1;
		a = 0;
		while (tmpX < this.board.size && this.board.map[y][tmpX].value == this.value)
		{
			a++;
			tmpX++;
		}
		type.a = a;
		type.b = b;
		type.total = a + b;

		// set value
		if (type.total > 0)
			this.setAxisCost(location, type, x, y);
	},

	checkYaxis: function(x, y) {
		// before
		var b;
		// after
		var a;
		var location = new Object();
		var type = new Object();
		var tmpY;

		// Number of location
		//Before
		tmpY = y - 1;
		b = 0;
		while (tmpY >= 0 && this.board.map[tmpY][x].value != this.opponent)
		{
			b++;
			tmpY--;
		}
		//After
		tmpY = y + 1;
		a = 0;
		while (tmpY < this.board.size && this.board.map[tmpY][x].value != this.opponent)
		{
			a++;
			tmpY++;
		}
		location.b = b;
		location.a = a;
		location.total = a + b + 1;

		// type
		tmpY = y - 1;
		b = 0;
		while (tmpY >= 0 && this.board.map[tmpY][x].value == this.value)
		{
			b++;
			tmpY--;
		}
		//After
		tmpY = y + 1;
		a = 0;
		while (tmpY < this.board.size && this.board.map[tmpY][x].value == this.value)
		{
			a++;
			tmpY++;
		}
		type.a = a;
		type.b = b;
		type.total = a + b;

		// set value
		if (type.total > 0)
			this.setAxisCost(location, type, x, y);
	},

	checkDiagLeftaxis: function(x, y) {
		// before
		var b;
		// after
		var a;
		var location = new Object();
		var type = new Object();
		var tmpX;
		var tmpY;

		// Number of location
		//Before
		tmpX = x - 1;
		tmpY = y - 1;
		b = 0;
		while (tmpX >= 0 && tmpY >= 0 && this.board.map[tmpY][tmpX].value != this.opponent)
		{
			b++;
			tmpX--;
			tmpY--;
		}
		//After
		tmpX = x + 1;
		tmpY = y + 1;
		a = 0;
		while (tmpX < this.board.size && tmpY < this.board.size && this.board.map[tmpY][tmpX].value != this.opponent)
		{
			a++;
			tmpX++;
			tmpY++;
		}
		location.b = b;
		location.a = a;
		location.total = a + b + 1;

		// Type
		// Before
		tmpX = x - 1;
		tmpY = y - 1;
		b = 0;
		while (tmpX >= 0 && tmpY >= 0 && this.board.map[tmpY][tmpX].value == this.value)
		{
			b++;
			tmpX--;
			tmpY--;
		}
		// After
		tmpX = x + 1;
		tmpY = y + 1;
		a = 0;
		while (tmpX < this.board.size && tmpY < this.board.size && this.board.map[tmpY][tmpX].value == this.value)
		{
			a++;
			tmpX++;
			tmpY++;
		}
		type.a = a;
		type.b = b;
		type.total = a + b;

		// set value
		if (type.total > 0)
			this.setAxisCost(location, type, x, y);
	},

	checkDiagRightaxis: function(x, y) {
		// before
		var b;
		// after
		var a;
		var location = new Object();
		var type = new Object();
		var tmpX;
		var tmpY;

		// Number of location
		//Before
		tmpX = x - 1;
		tmpY = y + 1;
		b = 0;
		while (tmpX >= 0 && tmpY < this.board.size && this.board.map[tmpY][tmpX].value != this.opponent)
		{
			b++;
			tmpX--;
			tmpY++;
		}
		//After
		tmpX = x + 1;
		tmpY = y - 1;
		a = 0;
		while (tmpX < this.board.size && tmpY >= 0 && this.board.map[tmpY][tmpX].value != this.opponent)
		{
			a++;
			tmpX++;
			tmpY--;
		}
		location.b = b;
		location.a = a;
		location.total = a + b + 1;

		// Type
		// Before
		tmpX = x - 1;
		tmpY = y + 1;
		b = 0;
		while (tmpX >= 0 && tmpY < this.board.size && this.board.map[tmpY][tmpX].value == this.value)
		{
			b++;
			tmpX--;
			tmpY++;
		}
		// After
		tmpX = x + 1;
		tmpY = y - 1;
		a = 0;
		while (tmpX < this.board.size && tmpY >= 0 && this.board.map[tmpY][tmpX].value == this.value)
		{
			a++;
			tmpX++;
			tmpY--;
		}
		type.a = a;
		type.b = b;
		type.total = a + b;

		// set value
		if (type.total > 0)
			this.setAxisCost(location, type, x, y);
	},

	setAxisCost: function(location, type, x, y) {
		var box = new Box();
		box.x = x;
		box.y = y;
		box.cost = 0;

		if (location.total >= 5 && (location.a - type.a > 0 && location.b - type.b > 0)) // Changed: adding >=
		{
			if (type.total == 1)
				box.cost = 20;
			else if (type.total == 2)
				box.cost = 30;
			else if (type.total == 3)
				box.cost = 39;
			else if (type.total >= 4)
				box.cost = 40;
			box.type = type.total;
			box.location = 2;
			this.boxList.push(box);
		}
		else if (location.total >= 5 && (location.a - type.a >= 0 || location.b - type.b >= 0)) // Changed: adding >=
		{
			if (type.total == 1)
				box.cost = 10;
			else if (type.total == 2)
				box.cost = 20;
			else if (type.total == 3)
				box.cost = 30;
			else if (type.total >= 4)
				box.cost = 40;
			box.type = type.total;
			box.location = 1;
			this.boxList.push(box);
		}
	}
}

module.exports = Heuristic;
