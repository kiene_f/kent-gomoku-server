Player = require('./Player.js');
Board = require('./Board.js');
Ruler = require('./Ruler.js');
MinMax = require('../AI/MinMax/MinMax.js');
AlphaBeta = require('../AI/AlphaBeta/AlphaBeta.js');
Utils = require('../utils.js');
Heuristic = require('./Heuristic.js');

function AI() {
	this.board;
	this.player;
	this.ruler = new Ruler();
	this.minmax = new MinMax();
	this.alphabeta = new AlphaBeta();
	this.heuristic = new Heuristic();
}

AI.prototype = {
	play: function(board, player)
	{
		console.time("Time AI");
		tmpBoard = new Board(board.size);
		tmpBoard.clone(board);
		this.board = board;
		this.player = player;
		var coord = {
			x: Math.floor(Math.random() * board.size),
			y: Math.floor(Math.random() * board.size)
		}
		var color = this.player.color;

		if (this.checkFirstMove(Utils.getReverseColor(player.color)) || this.checkFirstMove(player.color)) { // If each hasn't played yet
			coord = this.makeFirstMove(board);
		} else {
			// var box = this.minmax.play(board.clone(), color);
			var box = this.alphabeta.play(board.clone(), color);
			coord.x = box.x;
			coord.y = box.y;
		}
		console.timeEnd("Time AI");
		return coord;
	},

	checkFirstMove: function(color) {
		var firstMove = true;

		for (var y = 0; y < this.board.size; y++) {
			for (var x = 0; x < this.board.size; x++) {
				if ((color == "white" && this.board.map[y][x].value == 1) || (color == "black" && this.board.map[y][x].value == 2)) {
					firstMove = false;
				}
			}
		}
		return firstMove;
	},

	makeFirstMove: function(board) {
		if (this.checkFirstMove(Utils.getReverseColor(this.player.color))) {
			var coord = {
				x: Math.round(board.size / 2) -1,
				y: Math.round(board.size / 2) -1
			};
			return coord;
		} else {
			var coord = this.getPlayerCoord(Utils.getReverseColor(this.player.color));
			var coordList = this.getCoordListAroundCoord(board, coord);
			var rand = Math.floor(Math.random() * coordList.length);
			return coordList[rand];
		}
	},

	getCoordListAroundCoord: function(board, coord) {
		var coordList = [];

		if (coord.x - 1 >= 0 && coord.y - 1 >= 0)
			coordList.push(Utils.coord(coord.x - 1, coord.y - 1));
		if (coord.y -1 >= 0)
			coordList.push(Utils.coord(coord.x, coord.y - 1));
		if (coord.x + 1 < board.size && coord.y - 1 >= 0)
			coordList.push(Utils.coord(coord.x + 1, coord.y - 1));
		if (coord.x - 1 >= 0)
			coordList.push(Utils.coord(coord.x - 1, coord.y));
		if (coord.x + 1 < board.size)
			coordList.push(Utils.coord(coord.x + 1, coord.y));
		if (coord.x - 1 >= 0 && coord.y + 1 < board.size)
			coordList.push(Utils.coord(coord.x - 1, coord.y + 1));
		if (coord.y + 1 < board.size)
			coordList.push(Utils.coord(coord.x, coord.y + 1));
		if (coord.x + 1 < board.size && coord.y + 1 < board.size)
			coordList.push(Utils.coord(coord.x + 1, coord.y + 1));
		return coordList;
	},

	getPlayerCoord: function(color) {
		var firstMove = true;

		for (var y = 0; y < this.board.size; y++) {
			for (var x = 0; x < this.board.size; x++) {
				if ((color == "white" && this.board.map[y][x].value == 1) || (color == "black" && this.board.map[y][x].value == 2)) {
					var coord = {
						x: x,
						y: y
					}
					return coord;
				}
			}
		}
	}
}
module.exports = AI;
