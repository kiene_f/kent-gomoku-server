Game = require('./Game.js');

function Database(host, user, password, database) {
	this.host = host;
	this.user = user;
	this.password = password;
	this.database = database;
	this.connection;
}

Database.prototype = {
	create: function() {
		var mysql      = require('mysql');
		this.connection = mysql.createConnection({
		  host     : this.host,
		  user     : this.user,
		  password : this.password,
		  database : this.database
		});
	},
	insert: function(game, result) {
		var player1_id = null;
		var player2_id = null;
		var connection = this.connection;


		connection.query('INSERT INTO players SET name = ?, type = ?, color = ?', [game.player1.name, game.player1.type, game.player1.color], function (error, results, fields) {
			if (error) 
				throw error;
			player1_id = results.insertId;

			connection.query('INSERT INTO players SET name = ?, type = ?, color = ?', [game.player2.name, game.player2.type, game.player2.color], function (error, results, fields) {
				if (error) 
					throw error;
				player2_id = results.insertId;

				if (game.player1.color == result)
					connection.query('INSERT INTO games SET player_1 = ?, player_2 = ?, winner = ?', [player1_id, player2_id, player1_id], function (error, results, fields) {
						if (error) 
							throw error;
					});
				else
					connection.query('INSERT INTO games SET player_1 = ?, player_2 = ?, winner = ?', [player1_id, player2_id, player2_id], function (error, results, fields) {
						if (error) 
							throw error;
					});
			});
		});
	}
}

module.exports = Database;