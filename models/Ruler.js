function Ruler() {
}

Ruler.prototype = {
	checkMove: function (board, color, coord) {
		var win = "none";
        var result = {
            success: true,
            message: "Move: Move successful"
        }

		// Check if move is right ---
		if (this.checkEmptyBox(board, coord) == false) {
			result.success = false;
			result.message = "Move: Error - Box is not empty";
			console.log("Ruler: Box is not empty")
			// return result;
		} else {
			// IF RIGHT Make move ---
			board.addBox(coord, color);
			board.displayBoard(); // debugger
			result.success = true;

			// Check if win
			var win = this.checkWin(board);
			if (win == "white") {
				console.log("Ruler: White win"); // Debugger
				result.message = "Move: White player win !";
			} else if (win == "black") {
				console.log("Ruler: Black win"); // Debugger
				result.message = "Move: Black player win !";
			} else if (win == "draw") {
				console.log("Ruler: Draw"); // Debugger
				result.message = "Move: Draw !";
			}
		}
		console.log("Ruler: Game continues"); // Debugger
		return [result, win];
	},

	// Check win fcts -------

	checkDraw: function(board) {
		if (this.getNbrOfEmptyBox(board) == 0) {
			return true;
		}
		return false;
	},

	checkWin: function(board) {
		for(var y = 0; y < board.size; y++) {
			for(var x = 0; x < board.size; x++) {
				// If box is not empty
				if (typeof board.map[y][x] != 'undefined' && board.map[y][x].value > 0) {
					if ((result = this.checkXSide(board, x, y, board.map[y][x].value)) != "none") {
						console.log("Ruler: Win->checkXSide: " + result); // Debugger
						return result;
					} else if ((result = this.checkYSide(board, x, y, board.map[y][x].value)) != "none") {
						console.log("Ruler: Win->checkYSide: " + result); // Debugger
						return result;
					} else if ((result = this.checkDiagUpSide(board, x, y, board.map[y][x].value)) != "none") {
						console.log("Ruler: Win->checkDiagSide: " + result); // Debugger
						return result;
					} else if ((result = this.checkDiagDownSide(board, x, y, board.map[y][x].value)) != "none") {
						console.log("Ruler: Win->checkDiagSide: " + result); // Debugger
						return result;
					}
				}
			}
		}
		if (this.checkDraw(board)) {
			return "draw";
		}
		console.log("Ruler: No winner");
		return "none";
	},

	// Check 5 in a row on the X axe
	checkXSide: function(board, x, y, value) {
		var nbr = 0;

		for(var tmpX = x; tmpX < board.size; tmpX++) {
			// If box is not empty
			if (board.map[y][tmpX].value == value)
				nbr++;
			else
				return "none";
			// If 5 in a row
			if (nbr == 5) {
				if (value == 1)
					return "white";
				else if (value == 2)
					return "black";
			}
		}
		return "none";
	},

	// Check 5 in a row on the Y axe
	checkYSide: function(board, x, y, value) {
		var nbr = 0;

		for(var tmpY = y; tmpY < board.size; tmpY++) {
			// If box is not empty
			if (board.map[tmpY][x].value == value)
				nbr++;
			else
				return "none";
			// If 5 in a row
			if (nbr == 5) {
				if (value == 1)
					return "white";
				else if (value == 2)
					return "black";
			}
		}
		return "none";
	},

	// Check 5 in a row on the diagonal axe \
	checkDiagDownSide: function(board, x, y, value) {
		var nbr = 0;
		var tmpX = x;
		var tmpY = y;

		while (tmpX < board.size && tmpY < board.size) {
			// If box is not empty
			if (board.map[tmpY][tmpX].value == value)
				nbr++;
			else
				return "none";
			// If 5 in a row
			if (nbr == 5) {
				if (value == 1)
					return "white";
				else if (value == 2)
					return "black";
			}
			tmpX++;
			tmpY++;
		}
		return "none";
	},

	// Check 5 in a row on the diagonal axe /
	checkDiagUpSide: function(board, x, y, value) {
		var nbr = 0;
		var tmpY = y;
		var tmpX = x;

		while (tmpX >= 0 && tmpY < board.size) {
			// If box is not empty
			if (board.map[tmpY][tmpX].value == value)
				nbr++;
			else
				return "none";
			// If 5 in a row
			if (nbr == 5) {
				if (value == 1)
					return "white";
				else if (value == 2)
					return "black";
			}
			tmpY++;
			tmpX--;
		}
		return "none";
	},

	// Check if move is right fcts -------

	getNbrOfBox: function(board) {
		var nbr = 0;

		for(var y = 0; y < board.size; y++) {
			for(var x = 0; x < board.size; x++) {
				if (board.map[y][x].value > 0)
					nbr++;
			}
		}
		return nbr;
	},

	getNbrOfEmptyBox: function(board) {
		var nbr = 0;

		for(var y = 0; y < board.size; y++) {
			for(var x = 0; x < board.size; x++) {
				if (board.map[y][x].value == 0) {
					nbr++;
				}
			}
		}
		return nbr;
	},

	// Not used for the moment because we don't know which rule to implement
	checkFirstMove: function(board, color, coord) {
		if (this.getNbrOfBox(board) == 0)
			return true;
		return false;
	},

	checkEmptyBox: function(board, coord) {
		if (((coord.x >= 0 && coord.x < board.size) && (coord.y >= 0 && coord.y < board.size)) && typeof board.map[coord.y][coord.x] != 'undefined' && board.map[coord.y][coord.x].value == 0) {
			return true;
		}
		return false
	}
};

module.exports = Ruler;
