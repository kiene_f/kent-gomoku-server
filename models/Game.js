Player = require('./Player.js');
Board = require('./Board.js');
Ruler = require('./Ruler.js');
AI = require('./AI.js');
Utils = require('../utils.js');
Database = require('./Database.js');

function Game(id, type) {
	this.id = id;
	this.type = type;
	this.player1;
	this.player2;
	this.board;
	this.ruler = new Ruler();
	this.ai = new AI();
	this.database = new Database("localhost", "root", "", "gomoku");
}

Game.prototype = {
	reload: function() {
		this.updateNbrOfWin();
		this.resetData();
	},

	tryToReload: function(id) {
		var player = this.getOtherPlayerById(id);
	 	var send = {
		  reload: {
			Result: {
				wait: true,
				message: "Reload: Try to Reload"
			},
			Player: {
				id: player.id,
				color: player.color
			}
		  }
		}
		player.connection.send(JSON.stringify(send));
	},

	sendReload: function(player) {
	 	var send = {
		  reload: {
			Result: {
				success: true,
				message: "Reload: Reload successful"
			},
			Player: {
				id: player.id,
				color: player.color
			}
		  }
		}
		player.connection.send(JSON.stringify(send));
	},

	updateNbrOfWin: function() {
		if ((this.player1.color == "black" && this.player1.win == "black") || (this.player1.color == "white" && this.player1.win == "white"))
			this.player1.nbrWin++;
		if ((this.player2.color == "black" && this.player2.win == "black") || (this.player2.color == "white" && this.player2.win == "white"))
			this.player2.nbrWin++;
	},

	resetData: function() {
		var coord = {
			x: -1,
			y: -1
		}

		this.board.resetMap();
		this.player1.win = "none";
		this.player2.win = "none";
		if (this.player1.color == "black") {
			this.player1.color = "white";
			this.player2.color = "black";
			this.sendReload(this.player1);
			this.sendReload(this.player2);
			this.sendTurn(this.player2, coord);
		} else {
			this.player1.color = "black";
			this.player2.color = "white";
			this.sendReload(this.player1);
			this.sendReload(this.player2);
			this.sendTurn(this.player1, coord);
		}
	},

	// Create player1 or player2 regarding if player1 exist or not
	// Send to player a connection succeed message
	// Send to black player the "yourTurn" message to allow him to play first
	addPlayer: function (id, name, type, connection) {
		var result = {
			success: true,
			message: ""
		}
		if (typeof this.player1 === 'undefined') {
			this.player1 = new Player(id, name);
			this.player1.connection = connection;
			this.player1.type = type;
			this.chooseColor();
			result.message = "Connection: Game created !"
			this.sendConnection(result, this.player1);
			return this.player1;
		} else {
			this.player2 = new Player(id, name);
			this.player2.connection = connection;
			this.player2.type = type;
			this.chooseColor();
			result.message = "Connection: Game joined !"
			this.sendConnection(result, this.player2);

			var coord = {
				x: -1,
				y: -1
			}
			if (this.player1.color == "black")
				this.sendTurn(this.player1, coord);
			else
				this.sendTurn(this.player2, coord);
			return this.player2;
		}
	},

	sendTurn: function(player, coord) {
		if (player.type == "player")
		{
			var opponent = this.getOtherPlayerById(player.id);
		 	var send = {
			  yourTurn: {
				Coord: {
				  x: coord.x,
				  y: coord.y
				},
				Player: {
				  id: player.id,
				  win: player.win
				},
				Opponent: {
					name: opponent.name,
					color: opponent.color
				}
			  }
			}
			player.connection.send(JSON.stringify(send));
		}
		else if (player.win == "none")
			this.play(player.id, this.ai.play(this.board, player));
	},

	createBoard: function(size) {
		this.board = new Board(size);
		this.database.create();
	},

	chooseColor: function() {
		// If player1 exist and has no color assigned
		if (typeof this.player1 !== 'undefined' && typeof this.player1.color === 'undefined') {
			if (Math.floor(Math.random() * 2) == 0) {
				this.player1.color = "black";
			} else {
				this.player1.color = "white";
			}
		} else if (typeof this.player2 !== 'undefined' && typeof this.player2.color === 'undefined') {
			if (this.player1.color == "black") {
				this.player2.color = "white";
			} else if (this.player1.color == "white") {
				this.player2.color = "black";
			}
		}
	},

	//Make a move if possible. Send true if move if good, send false is move is impossible
	play: function(playerId, coord) {
		var player = this.getPlayerById(playerId);
		var newBoard = this.board.clone();
		var result = this.ruler.checkMove(newBoard, player.color, coord, playerId);

		if (result[0].success) {
			console.log("Move successful");
			this.board.addBox(coord, player.color);
		}
		var send = {
			move: {
				Result: result[0],
				Coord: coord,
				Player: {
					id: player.id,
					win: result[1]
				}
		 	}
		}
		player.connection.send(JSON.stringify(send));

		if (result[0].success) {
			var secondPlayer = this.getOtherPlayerById(player.id);
			secondPlayer.win = result[1];
			this.sendTurn(secondPlayer, coord);
		}

		if (result[1] != "none")
			this.database.insert(this, result[1]);
	},

	getPlayerById: function(id) {
		if (this.player1.id == id)
			return this.player1;
		else
			return this.player2;
	},

	getOtherPlayerById: function(id) {
		if (this.player1.id != id)
			return this.player1;
		else
			return this.player2;
	},

	sendConnection: function(result, player) {

		var opponent = this.getOtherPlayerById(player.id);
		if (typeof opponent == 'undefined')
		{
			opponent = new Object();
			opponent.name = "none";
		}
		var send = {
		  connection: {
			Result: result,
			Player: {
			  id: player.id,
			  color: player.color
			},
			Opponent: {
				name: opponent.name,
				color: opponent.color
			}
		  }
		}
		player.connection.send(JSON.stringify(send));
	}

};

module.exports = Game;
